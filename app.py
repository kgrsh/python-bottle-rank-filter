from bottle import route, run, template, request, response
from PIL import Image
from html import escape
from itertools import chain
from io import BytesIO
from traceback import print_exc
from base64 import b64encode
from hashlib import sha1
from time import time
from random import random
import imghdr
import os 

class RankFilter():
    def __init__(self,array,padding,offset):
        self.original_array=array                                               
        self.new_array=[[ None for _ in row] for row in self.original_array]    
        self.xs=len(self.original_array[0])                                     
        self.ys=len(self.original_array)                                        
        self.padding=padding                                                    
        self.mask_size=self.padding*2+1                                         
        self.offset=offset                                                      
        self.padded_array=pad_array(self.original_array,padding)                
        print("Launching RankFilter with mask size={}x{} offset={}"\
            .format(self.mask_size, self.mask_size, self.offset))              
        self.filter()
    
    def filter(self):
        for y in range(self.ys):
            for x in range(self.xs):
                segment_2d=extract_segment(self.padded_array, y, x,self.mask_size)  
                segment_1d=flattened_and_sorted(segment_2d)                         
                self.new_array[y][x]=pick_closer(segment_1d,                        
                        self.original_array[y][x],self.offset)                      
    
    def getdata_2d(self):
        return self.new_array

    def getdata_1d(self):
        return list(chain.from_iterable(self.new_array))

def pad_array(array,padding):
    array=[[row[0] for _ in range(padding)] + row +\
        [row[-1] for _ in range(padding)] for row in array]
    array=[array[0] for _ in range(padding)] + array +\
        [array[-1] for _ in range(padding)]
    return array

def extract_segment(array,y,x,l):
    return [row[x:x+l] for row in array[y:y+l]]

def flattened_and_sorted(array):
    return sorted(chain.from_iterable(array))

def pick_closer(array,value,offset):
    assert len(array) % 2 == 1
    assert offset <= (len(array) // 2 + 1)
    if abs(value - array[ offset-1 ]) < abs(value - array[ -offset ]):
        return array[offset-1]
    else:
        return array[-offset]

def encodebase64(filename):
    print("Encoding {}".format(filename))
    with open(filename,'rb') as file_:
        buf=file_.read()
    print(len(buf))
    return b64encode(buf)

def generate_user_id(ip):
    return sha1(bytes(str(time())+str(ip)+str(random()),'ascii')).hexdigest()

def hash_file(filename):
    data=True
    with open(filename,'rb') as infile:
        hasher=sha1()
        while data:
            data=infile.read(16384)
            hasher.update(data)
    return hasher.hexdigest()

def number_to_line(number,length,maximum):
    assert length < maximum
    filled_length=number/maximum*length
    filled_pixels=min(int(filled_length),length-1)
    empty_pixels=length-filled_pixels-1
    partial_pixel_brightness=(1-(filled_length % 1))*255
    ret_list = [0 for i in range(filled_pixels)]+\
        [int(partial_pixel_brightness*255)]+[255 for i in range(empty_pixels)]
    assert len(ret_list)==length
    return ret_list

def list_to_picture(brightness_list,height):
    hist_io=BytesIO()
    hist=Image.new('L',(height,len(brightness_list)),128)
    brightness_2d_list=[number_to_line(brightness,height,max(brightness_list))
        for brightness in brightness_list ]
    hist.putdata(list(chain.from_iterable(brightness_2d_list)))
    hist=hist.rotate(90,expand=1)
    hist.save(hist_io,format='png')
    return b64encode(hist_io.getvalue())

def image_slice(img,slice_v,slice_h):
    data=list(img.getdata())
    warnings=""
    if slice_v>=img.width:
        warnings+="Selected column is outside the image, using {} instead <br>".format(img.width-1)
        slice_v=img.width-1
    if slice_h>=img.width:
        warnings+="Selected row is outside the image, using {} instead <br>".format(img.height-1)
        slice_h=img.height-1
    if slice_v>=0:
        if slice_h>=0:
            return 'Value at y: {}, x: {} = {}'.format(slice_v,
                slice_h,img.getpixel((slice_h,slice_v)))
        else:
            assert img.height==len(data[slice_v::img.width])
            return warnings+'Values in column {slice_v} <br> <img style="border: 1px solid black"'\
            'src="data:image/png;base64,{b64}" title="{title}" alt="{title}">'.format(
            slice_v=slice_v,
            b64=list_to_picture(data[slice_v::img.width],128).decode(),
            title=str(data[slice_v::img.width])
            )
    else:
        if slice_h>=0:
            assert img.width==len(data[slice_h*img.width:(slice_h+1)*img.width])
            return warnings+'Values in row {slice_h} <br> <img style="border: 2px solid black"'\
            'src="data:image/png;base64,{b64}" title="{title}" alt="{title}">'.format(
            slice_h=slice_h,
            b64=list_to_picture(data[slice_h*img.width:(slice_h+1)*img.width],128).decode(),
            title=str(data[slice_h*img.width:(slice_h+1)*img.width])
            )
        else:
            return ''

@route('/',method=['GET','POST'])
def index():
    script_name=request['SCRIPT_NAME']          
    header_string=""
    table_string=""
    file_info_string=""
    slice_old_string=""
    slice_new_string=""
    S=3
    padding=(S-1)//2
    k=1
    slice_v=-1
    slice_h=-1
    im_o=Image.new('L',(8,8),128)

    if 'user_id' in dict(request.cookies):                                      
        user_id=request.cookies['user_id']
        print('Reusing user_id',user_id,'for ip',request['REMOTE_ADDR'])

    else:
        user_id=generate_user_id(request['REMOTE_ADDR'])                        
        response.set_cookie('user_id',user_id)
        print('Setting new user_id',user_id,'for ip',request['REMOTE_ADDR'])
    tmp_filename=os.path.join(os.getcwd(),'uploads',user_id+'.bin')

    if 'pic' in dict(request.files):                                            
        if request.files['pic']:
            request.files['pic'].save(tmp_filename,overwrite=True)
    if 'S' in dict(request.params):
        S=int(request.params['S'])
        padding=(S-1)//2
    if 'k' in dict(request.params):
        k=int(request.params['k'])
        if k>(S*S+1)//2:
            header_string="Значение k слишком велико "\
            "и было снижено до максимума ({}) <br> ".format((S*S+1)//2)
            k=(S*S+1)//2
    if 'slice_v' in dict(request.params):
        if request.params['slice_v']:
            slice_v=int(request.params['slice_v'])
    if 'slice_h' in dict(request.params):
        if request.params['slice_h']:
            slice_h=int(request.params['slice_h'])

    if os.path.isfile(tmp_filename):                                                            
        parameter_string='file={}_S={}_k={}'.format(hash_file(tmp_filename),S,k)                
        res_filename=os.path.join(os.getcwd(),'uploads',user_id+'.'+parameter_string+'.bin')    
        try:

            if not os.path.isfile(res_filename) :                                               
                im_o=Image.open(tmp_filename).convert('L')                                      
                im_o.format=imghdr.what(tmp_filename)                                           
                im_o_data=list(im_o.getdata())                                                  
                im_o_2d_data=[im_o_data[i:i+im_o.size[0]] for i in range(0,im_o.size[0]*im_o.size[1],
                    im_o.size[0])]
                rf=RankFilter(im_o_2d_data,padding,k)                                           
                im_n=im_o.copy()                                                                
                im_n.format=im_o.format
                im_n.putdata(rf.getdata_1d())                                                   
                im_n.save(res_filename,format=im_n.format)
            else:
                im_o=Image.open(tmp_filename).convert('L')
                im_n=Image.open(res_filename).convert('L')
            slice_old_string=image_slice(im_o,slice_v,slice_h)
            slice_new_string=image_slice(im_n,slice_v,slice_h)

            print(str(im_o))
            with open(os.path.join(os.getcwd(),'templates',
                'template_table.html'),encoding='utf8') as template_file:
                table_string = template(template_file.read(),
                    image_format=im_o.format,
                    base64_old=encodebase64(tmp_filename),
                    base64_new=encodebase64(res_filename),
                    hist_old=str(im_o.histogram()),
                    hist_new=str(im_n.histogram()),
                    base64_hist_old=list_to_picture(im_o.histogram(),128),
                    base64_hist_new=list_to_picture(im_n.histogram(),128),
                    slice_old=slice_old_string,
                    slice_new=slice_new_string,

                    )
        except Exception as excp:
            print_exc()
            header_string="Error while processing file!"
    else:
        header_string="Upload a file!"
            

    with open(os.path.join(os.getcwd(),'templates','template_main.html'),encoding='utf8') as template_file:
        return template(template_file.read(),header=header_string,
            script_name=script_name,table=table_string,
            current_S=S,current_k=k,
            file_info=file_info_string,
            slice_v=slice_v,slice_h=slice_h,
            )

os.makedirs("uploads",exist_ok=True)
run(host='localhost', port=8081)
